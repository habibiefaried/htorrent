/*****
Created By : Habibie Faried
Date : 13 Juni 2013
Deskripsi : File ini merupakan algoritma list user dasar
untuk melist setiap peer yang online pada tracker
******/

class ElmtListUser
{
	private int ID;
	private String IPAddress;
	private String Nama;
	private int Port;
	private ElmtListUser next;

	public ElmtListUser(int ID, String IPAddress, String Nama, int Port)
	{
		this.ID = ID;
		this.IPAddress = IPAddress;
		this.Nama = Nama;
		this.Port = Port;
		next = null;
	}

	public void setNext(ElmtListUser next) { this.next = next; }
	public void setID(int ID) { this.ID = ID; }
	public void setIP(String IP) { this.IPAddress = IP; }
	public void setNama(String Nama) { this.Nama = Nama; }
	public void setPort(int Port) { this.Port = Port; }

	public int getID() { return ID; }
	public String getIP() { return IPAddress; }
	public String getNama() { return Nama; }
	public int getPort() { 	return Port; }
	public ElmtListUser getNext() { return next; }
}

class ListUser
{
	private static ElmtListUser First;
	
	public ListUser()
	{
		First = null;
	}

	public static boolean isEmpty()
	{
		return (First == null);
	}

	public static ElmtListUser getFirst(){return First;}

	public static void InsertFirst(int ID, String IPAddress, String Nama, int Port)
	{
		ElmtListUser e = new ElmtListUser(ID,IPAddress,Nama,Port);
		e.setNext(First);
		First = e;
	}

	public static void Delete(int ID)
	{
		//ID dijamin unik
		ElmtListUser P = First;
		ElmtListUser Prev = null;
		if (!isEmpty())
		{
			if (First.getID() == ID) //jika ketemu pada pertama
			{
				Prev = DeleteFirst();
			}
			else
			{
				Prev = P;
				P = P.getNext(); //mulai cek kedua
				while (P != null)
				{
					if (P.getID() == ID)
					{
						Prev.setNext(P.getNext());
					}
					Prev = P;
					P = P.getNext(); //mulai cek kedua
				}
			}
		}	
	} //implementasikan ini

	public static ElmtListUser DeleteFirst()
	{
		ElmtListUser e = null;
		if (!isEmpty())
		{
			e = First;
			First = First.getNext();
		}
		return e;
	}

	public static void print()
	{
		ElmtListUser P = First;
		System.out.println("==================================================");		
		System.out.println("ID	Nama		Alamat		Port");
		System.out.println("==================================================");
		while (P != null)
		{
			System.out.println(P.getID()+"	"+P.getNama()+"		"+P.getIP()+"	"+P.getPort());
			P = P.getNext();
		}
		System.out.println("==================================================");
	}

	public static String SearchIP(int ID)
	{
		ElmtListUser P = First;
		boolean isKetemu = false;
		String ret = "NULL";
	
		while ((P != null) && (!isKetemu))
		{
			if (P.getID() == ID)
			{
				isKetemu = true;
				ret = P.getIP();
			}
			P = P.getNext();
		}
		return ret;
	}

	public static int SearchPort(int ID)
	{
		ElmtListUser P = First;
		boolean isKetemu = false;
		int ret = 0;
	
		while ((P != null) && (!isKetemu))
		{
			if (P.getID() == ID)
			{
				isKetemu = true;
				ret = P.getPort();
			}
			P = P.getNext();
		}
		return ret;
	}

	public static String SearchNama(int ID)
	{
		ElmtListUser P = First;
		boolean isKetemu = false;
		String ret = "NULL";
	
		while ((P != null) && (!isKetemu))
		{
			if (P.getID() == ID)
			{
				isKetemu = true;
				ret = P.getNama();
			}
			P = P.getNext();
		}
		return ret;
	}
}

public class MainListUser
{
	public static void main(String args[])
	{
		/*
		new ListUser();
		ListUser.InsertFirst(1,"localhost","Habibie",8080);
		ListUser.InsertFirst(2,"localhost22","Habibie",7999);
		System.out.println(ListUser.SearchIP(2));
		ListUser.DeleteFirst();
		ListUser.print();
		*/
	}
}


		


