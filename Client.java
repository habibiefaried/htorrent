/*****
Created By : Habibie Faried
Date : 16 Juni 2013
Deskripsi : File ini merupakan client untuk program hTorrent
******/

import java.net.*;
import java.io.*;
import java.util.*;

//Integer.parseInt(str)
//coba.toString() //coba harus integer
//Port tracker di 5050
class Dest
{
	//Kelas ini merupakan tempat download file
	private int No;
	private String IP;
	private int Port;
	private String HashFile;
	private Dest next;

	public Dest(int No, String IP, int Port, String HashFile)
	{
		this.No = No;
		this.IP = IP;
		this.Port = Port;
		this.HashFile = HashFile;
		this.next = null;	
	}

	public void setNext(Dest next){this.next = next;}
	
	public Dest getNext(){return this.next;}
	public String getIP() {return this.IP;}
	public int getPort() {return this.Port;}
	public String getHashFile(){return this.HashFile;}

	public void Download(Socket S,String nama,String HashFile, int JmlPart,String FileName)
	{ 
		int part = No; // sama seperti nomor
		Algorithm A = new Algorithm(S);
		A.send(nama);
		A.send(HashFile);
		Integer I = new Integer(part);
		A.send(I.toString());
		I = new Integer(JmlPart);
		A.send(I.toString());
		System.out.println("File akan disimpan kedalam "+FileName+".sp"+part);
		System.out.println("Sedang menerima file. . .");
		A.recvFile(FileName+".sp"+part);
		System.out.print("hTorrent> ");
	}
}

class TransferFile extends Thread
{
	private Dest D;
	private int Top; //banyaknya seeder yang memiliki file
	private String nama; //nama user
	private String FileName; //nama file target (Full path)
	private String IPTracker; //jika IP yang didapat adalah 127.0.0.1, harus diganti ke IP Tracker
	private String NamaFile; //nama file target (nama file saja, non-full path)
	private Algorithm A;

	public TransferFile(Algorithm A, String nama, String NamaFile, String PathFile, String IPTracker)
	{
		D = null;
		Top = 0;
		this.nama = nama;
		this.FileName = PathFile;
		this.IPTracker = IPTracker;
		this.NamaFile = NamaFile;
		this.A = A;
	}

	public void InsertTorrent(String S)
	{
		StringTokenizer st = new StringTokenizer(S, "-");
		String IP = st.nextToken();
		String ports = st.nextToken();
		int port = Integer.parseInt(ports);
		String HashFile = st.nextToken();
		Insert(IP,port,HashFile);
	}

	public void Insert(String IP, int Port, String HashFile)
	{
		Top++; //increment jumlah
		if (IP.equals("127.0.0.1")) //jika IP adalah 127.0.0.1
		{
			IP = IPTracker; //ganti dengan IPTracker
		}

		Dest P = new Dest(Top,IP,Port,HashFile);
		P.setNext(D);
		D = P;
	}

	public boolean isEmpty(){return (D == null);}
	
	public void run() //pada download, ini fungsi thread
	{
		if (!isEmpty())
		{
			int i = 1;
			Dest P = D;
			while (P != null)
			{
				try
				{
					Socket client = new Socket(P.getIP(),P.getPort());
					P.Download(client,nama,P.getHashFile(),Top,FileName);
					i++; //increment file
					P = P.getNext();
				}catch (Exception e){}			
			}
			A.mergeFile(FileName);
			ListFile.InsertFirst(NamaFile,FileName); //Selesai download, masukkan ke list file
			
			A.DeleteAllSplitFile(FileName);
			/* Setor semua list file kepada tracker */
			A.send("StoreList");			
			ElmtListFile e = ListFile.getFirst(); //P merupakan head dari ElmtListFile
			while (e != null)
			{
				A.send(e.getName()); //kirim nama file
				A.send(e.getHash()); //kirim hash file
				e = e.getNext();			
			}
			A.send("Done");			
			/* End of Setor */		
		}
	}
}


class ClientThread extends Thread
{
	private ServerSocket serverSocket;
	private int ID;
	private String nama;

	public ClientThread (ServerSocket serverSocket, int ID)
	{
		this.serverSocket = serverSocket;
		this.ID = ID;
	}

	public void run()
	{
		try
		{
			String data;

			Socket server = serverSocket.accept();
			Algorithm A = new Algorithm(server);
			System.out.println("Connected to "+ server.getInetAddress().getHostAddress());
			nama = A.recv();
			String HashFile = A.recv();
			int part = Integer.parseInt(A.recv());
			int jmlpart = Integer.parseInt(A.recv());
			System.out.println("Koneksi ini dilakukan oleh : "+nama);
			String FileName = ListFile.SearchName(HashFile);
			String FilePath = ListFile.SearchPath(HashFile);
			System.out.println("File yang dimiliki : "+FileName+ "berada pada "+FilePath);
			long FileSize = A.CountSize(FilePath);
			System.out.println("File memiliki ukuran : "+FileSize);
			System.out.println("File yang diminta adalah part "+part+" dari semua part ada "+jmlpart);
		
			long PartSize = FileSize / jmlpart;
			PartSize = PartSize + 10; //kasih redundant 10 byte
 
			A.splitFile(FilePath,PartSize,part);
			A.sendFile(FilePath+".sp"+part); //kirim file hasil split
			A.DeleteAllSplitFile(FilePath);

			System.out.println("File bernama "+FileName+".sp"+part+" sudah dikirim kepada "+nama);			

			System.out.print("hTorrent> ");
			server.close();
		}
		catch (Exception e)
		{	System.out.println(e.getMessage());
		}
	}
}

public class Client
{
	private static String[] baris = new String[100];

	public static void ReadTorrent(String TrFile)
	{
		int i;
		for (i=0;i<100;i++) baris[i] = null; //inisialisasi

		i = 0;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(TrFile));
			while ((baris[i] = br.readLine()) != null) //baca baris per line teks
			{
				i++;
			}
		}
		catch (Exception e){System.out.println(e.getMessage()); }
	}
	
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		String IPTracker;
		String Nama;
		String Location;
		String data; String command;
		int i = 1; int Jml; boolean isDone = false;
		System.out.print("Masukkan Nama anda : "); Nama = sc.nextLine(); 	
		System.out.print("Masukkan IP Address Tracker : "); IPTracker = sc.nextLine();
		System.out.print("Masukkan lokasi direktori file anda : "); Location = sc.nextLine();
		System.out.print("Masukkan jumlah thread yang ingin anda jalankan : "); Jml = sc.nextInt();
		boolean Error = true;
		Integer port = new Integer(5030);
		try
		{
			Socket client = new Socket(IPTracker,5050);
			Algorithm C = new Algorithm(client);
			C.cmd("clear");
			/* Pembentukan untuk menjadi server pada peer lain */			
			while (Error)
			{
				try
				{
					ServerSocket serverSocket = new ServerSocket(port);
					Error = false;
					System.out.println("Listening peer @ "+port);
					while (i <= Jml)
					{
						new ClientThread(serverSocket,i).start();
						i++;
					}
					
				}
				catch (Exception e){port++;}
			}
			/* End of this section */

			/* Read File */
			new ListFile();
			try
			{
				ListFile.ReaderProcess(new File(Location));
			}catch(Exception e){System.out.println("Reader File Error : "+e.getMessage()); }
			/* End of Read File */

			/* Penyambungan ke tracker */
			C.send(Nama);
			C.send(port.toString());
			System.out.println(C.recv());
			/* End */

			/* Setor semua list file kepada tracker */
			ElmtListFile P = ListFile.getFirst(); //P merupakan head dari ElmtListFile
			while (P != null)
			{
				C.send(P.getName()); //kirim nama file
				C.send(P.getHash()); //kirim hash file
				P = P.getNext();			
			}
			C.send("Done");			
			/* Sekuens perintah selanjutnya */

			while (!isDone)
			{
				try
				{
					System.out.print("hTorrent> ");  command = sc.nextLine();
					if (command.equals("quit"))
					{
						isDone = true;
						C.send("quit");
						System.exit(0);
					}
					else if (command.equals("ListMyFile"))
					{
						ListFile.print();
					}
					else if (command.equals("ListFile"))
					{
						C.send("ListFile");
						String NameFile = C.recv();
						String No = C.recv();
						System.out.println("======================");
						while (!(NameFile.equals("ListDone")))
						{
							System.out.println(No+". "+NameFile);
							NameFile = C.recv();
							No = C.recv();
						}
						System.out.println("======================");				
					}
					else if (command.equals("download"))
					{
						System.out.print("Masukkan ID File (atau -1 untuk batal) : ");
						Integer IDFile = new Integer(sc.nextInt());
						if (IDFile != -1)
						{
							C.send("download"); C.send(IDFile.toString());
							String FileName = C.recv();
							System.out.println("Command download file "+FileName+" telah diterima"); //nama file
							String FullPath = C.recvFile(Location+"/"+FileName+".tr");
							ReadTorrent(FullPath); //Baca file torrent, lalu download filenya
							int Ln = 0;
							
							TransferFile T = new TransferFile(C,Nama,FileName,Location+"/"+FileName,IPTracker); //transfer file
							while (baris[Ln] != null)
							{				
								System.out.println(baris[Ln]);
								T.InsertTorrent(baris[Ln]);
								Ln++;
							}
							T.start(); //jalankan download
							//File f = new File(FullPath);						
							//f.delete(); //delete file torrent
						}					
					}
					else
					{
						System.out.println("");
					}			
				} catch (Exception e){ isDone = true; }
			}
					
			/* End of Sekuens */
			client.close(); //tutup koneksi
		}catch (Exception e ) { System.out.println(e.getMessage()); }	
	}
}

