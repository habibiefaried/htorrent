/*****
Created By : Habibie Faried
Date : 14 Juni 2013
Deskripsi : File ini merupakan algoritma list file dasar
untuk hTorrent Client
******/

import java.io.*;
import java.math.*;
import java.security.*;

class ElmtListFile
{
	private int ID;
	private String NameFile;
	private String FilePath;
	private String HashFile;
	private ElmtListFile next;

	public ElmtListFile(int ID, String NameFile, String FilePath)
	{
		this.ID = ID;
		this.NameFile = NameFile;
		this.FilePath = FilePath;
		next = null;
	}

	public void setNext(ElmtListFile next) { this.next = next; }
	public void setID(int ID) { this.ID = ID; }
	public void setName(String Name) { this.NameFile = Name; }
	public void setPath(String Path) { this.FilePath = Path; }
	public void setHash()
	{
			try
			{
				MessageDigest digest = MessageDigest.getInstance("MD5");
				File f = new File(FilePath);
				InputStream is = new FileInputStream(f);                
				byte[] buffer = new byte[8192];
				int read = 0;
				try {
				 	while( (read = is.read(buffer)) > 0) {
						  digest.update(buffer, 0, read);
					 }       
				 byte[] md5sum = digest.digest();
				 BigInteger bigInt = new BigInteger(1, md5sum);
				 String output = bigInt.toString(16);
				 HashFile = output;
				}
				catch(IOException e) {
					 throw new RuntimeException("Unable to process file for MD5", e);
				}
				finally {
				 	try {
					  is.close();
				 	}
				 	catch(IOException e) {
						  throw new RuntimeException("Unable to close input stream for MD5 calculation", e);
				 	}
				}
			}catch (Exception e) { System.out.println(e.getMessage()); }
	}   

	public int getID() { return ID; }
	public String getName() { return NameFile; }
	public String getPath() { return FilePath; }
	public String getHash() { return HashFile; }
	public ElmtListFile getNext() { return next; }
}

class ListFile
{
	private static ElmtListFile First;
	private static int Top; //untuk ID

	public ListFile()
	{
		First = null;
		Top = 0;
	}

	public static ElmtListFile getFirst(){return First;}
	public static boolean isEmpty()
	{
		return (First == null);
	}

	public static void InsertFirst(String Name, String Path)
	{
		Top++;
		ElmtListFile e = new ElmtListFile(Top,Name,Path);
		e.setNext(First);
		e.setHash(); //set Hash File
		First = e;
	}

	public static ElmtListFile DeleteFirst()
	{
		ElmtListFile e = null;
		if (!isEmpty())
		{
			Top--;
			e = First;
			First = First.getNext();
		}
		return e;
	}

	public static void print()
	{
		ElmtListFile P = First;
		System.out.println("==================================================");		
		System.out.println("ID	Nama		md5");
		System.out.println("==================================================");
		while (P != null)
		{
			System.out.println(P.getID()+"	"+P.getPath()+"		"+P.getHash());
			P = P.getNext();
		}
		System.out.println("==================================================");
	}

	public static String SearchName(String HashFile)
	{		
		ElmtListFile P = First;
		boolean isKetemu = false;
		String ret = "NULL";
	
		while ((P != null) && (!isKetemu))
		{
			if (P.getHash().equals(HashFile))
			{
				isKetemu = true;
				ret = P.getName();
			}
			P = P.getNext();
		}
		return ret;
	}

	public static String SearchPath(String HashFile)
	{		
		ElmtListFile P = First;
		boolean isKetemu = false;
		String ret = "NULL";
	
		while ((P != null) && (!isKetemu))
		{
			if (P.getHash().equals(HashFile))
			{
				isKetemu = true;
				ret = P.getPath();
			}
			P = P.getNext();
		}
		return ret;
	}


	public static String SearchName(int ID)
	{
		ElmtListFile P = First;
		boolean isKetemu = false;
		String ret = "NULL";
	
		while ((P != null) && (!isKetemu))
		{
			if (P.getID() == ID)
			{
				isKetemu = true;
				ret = P.getName();
			}
			P = P.getNext();
		}
		return ret;
	}

	public static String SearchPath(int ID)
	{
		ElmtListFile P = First;
		boolean isKetemu = false;
		String ret = "NULL";
	
		while ((P != null) && (!isKetemu))
		{
			if (P.getID() == ID)
			{
				isKetemu = true;
				ret = P.getPath();
			}
			P = P.getNext();
		}
		return ret;
	}

	public static void setHashFile(int ID)
	{
		ElmtListFile P = First;
		boolean isKetemu = false;
	
		while ((P != null) && (!isKetemu))
		{
			if (P.getID() == ID)
			{
				isKetemu = true;
				P.setHash();
			}
			P = P.getNext();
		}
	}		

	public static String getHashFile(int ID)
	{
		ElmtListFile P = First;
		boolean isKetemu = false;
		String hash="NULL";

		while ((P != null) && (!isKetemu))
		{
			if (P.getID() == ID)
			{
				isKetemu = true;
				hash = P.getHash();
			}
			P = P.getNext();
		}
		return hash;
	}	

	public static void ReaderProcess(File aFile)
	{
		try
		{
			if(aFile.isFile()) //jika File
			{
				InsertFirst(aFile.getName(),aFile.getCanonicalPath());
			}
			else if (aFile.isDirectory())
			{ //jika Directory
				File[] listOfFiles = aFile.listFiles();
				if(listOfFiles!=null) {
					for (int i = 0; i < listOfFiles.length; i++) ReaderProcess(listOfFiles[i]); //rekursi
		   	}
		 	}
		}
		catch (Exception e) { System.out.println(e.getMessage()); }
	}
}

public class MainListFile
{
	public static void main(String args[])
	{
		new ListFile();	
		try
		{
			ListFile.ReaderProcess(new File(new java.io.File( "." ).getCanonicalPath()));
		}catch(Exception e){}
		System.out.println(ListFile.SearchName(2));
		ListFile.setHashFile(3);
		System.out.println(ListFile.getHashFile(3));
		ListFile.DeleteFirst();
		ListFile.print();
	}
}


		


