/*****
Created By : Habibie Faried
Date : 12 Juni 2013
Deskripsi : File ini merupakan algoritma socket dasar yang digunakan untuk
membuat program hTorrent
******/

import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.lang.*;

public class Algorithm
{
	private Socket So;
	private Runtime rt;

	public Algorithm()
	{
		this.So = null;
		this.rt = null;	
	}

	public Algorithm(Socket So)
	{
		this.So = So;
	}

	public void cmd(String command)
	{
		try
		{
			rt = Runtime.getRuntime();
			rt.exec(command);
		}catch (Exception e){System.out.println(e.getMessage()); }
	}

	public void send(String S)
	{
		//Prosedur ini untuk mengirimkan data berupa string
		try
		{
			DataOutputStream out =  new DataOutputStream(So.getOutputStream());
		   out.writeUTF(S);		
		}	
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public String recv()
	{
		//Prosedur ini untuk menerima data berupa string, mengembalikan string
		try
		{
			DataInputStream in = new DataInputStream(So.getInputStream());
			return in.readUTF();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		return "NULL";
	}

	public void sendFile(String LocationFile)
	{
		//Algoritma ini digunakan untuk mengirimkan File, terhubung ke ByteStream.java
		try
		{
			OutputStream os = So.getOutputStream();
			ByteStream.toStream(os,new File(LocationFile)); //kirim file
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public String recvFile(String LocationDir)
	{
		//Algoritma ini digunakan untuk menerima file, terhubung ke ByteStream.java
		//Masukan berupa path untuk menerima file, mengembalikan file_name
		try
		{
			InputStream in = So.getInputStream();
			String file_name;
			file_name = LocationDir; //+file_name tadinya
  			File file=new File(file_name);
  			ByteStream.toFile(in, file); //menerima dan menulis data
			return file_name;
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		return "NULL";
	}

	public void splitFile(String FilePath, long splitlen, int No)
	{
		//Algoritma ini digunakan untuk memecah file, masukan berupa
		//Nama file yang diminta, nama file yang sebenarnya, ukuran file split (dalam byte) dan nomor split yang diminta

		long leninfile = 0, leng = 0;
		int count = 1, data;
		//splitlen = splitlen * 1024 * 1024; //jika ingin dalam MB
		try
		{
			File filename = new File(FilePath);
			InputStream infile = new BufferedInputStream(new FileInputStream(filename));
			data = infile.read();

			while (data != -1)
			{
				//Disini algoritma split file
				filename = new File(FilePath + ".sp" + count);
				OutputStream outfile = new BufferedOutputStream(new FileOutputStream(filename));
				while (data != -1 && leng < splitlen)
				{
					if ((count == No) || (No == -1)) //-1 artinya semuanya di split
						outfile.write(data);
					
					leng++;
					data = infile.read();
				}
				leninfile += leng;
				leng = 0;
				outfile.close(); //close data per split
				count++;
			}
		} catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public void DeleteAllSplitFile(String FilePath)
	{
		//delete semua split file
		int count = 1;
		File filename = new File(FilePath);
		while (true)
		{
			filename = new File(FilePath+".sp"+count);
			if (filename.exists())
			{
				filename.delete();
				count++; //increment
			}
			else break;
		}
	}

	public void mergeFile(String FilePath)
	{
		//Prosedur ini digunakan untuk menggabungkan file, masukkan nama filenya
		//Lalu prosedur ini akan mencari file pecahan secara otomatis
		long leninfile = 0, leng = 0;
		int count = 1, data = 0;
		try
		{
			File filename = new File(FilePath);
			OutputStream outfile = new BufferedOutputStream(new FileOutputStream(filename)); //nama file target
			while (true)
			{
				filename = new File(FilePath + ".sp" + count);
				if (filename.exists())
				{
					
					InputStream infile = new BufferedInputStream(new FileInputStream(filename));
					data = infile.read(); //baca file
					while (data != -1) {
						outfile.write(data);
						data = infile.read(); //gabungkan ke file gabungan
					}
					leng++;
					infile.close(); //close data (per split)
					count++;
					} else {
						break;
				}
				filename.delete();
			}
			outfile.close(); //close file hasil gabungan
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public long CountSize(String FilePath)
	{
		long leng = 0;
		int data;
		try
		{
			File filename = new File(FilePath);
			InputStream infile = new BufferedInputStream(new FileInputStream(filename));
			data = infile.read();

			while (data != -1)
			{
				leng++;
				data = infile.read();
			}
			return leng;
		}catch (Exception e){System.out.println(e.getMessage());}
		return 0;
	}
}

