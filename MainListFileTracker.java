/*****
Created By : Habibie Faried
Date : 14 Juni 2013
Deskripsi : File ini merupakan algoritma list file dasar
untuk hTorrent Tracker
******/

import java.io.*;
import java.lang.*;
import java.util.*;

class Belong
{
	private int ID; //ID peer
	private Belong next;
	
	public Belong(int ID)
	{
		this.ID = ID;
		next = null;
	}

	public void setID(int ID) { this.ID = ID; }
	public void setNext(Belong next) { this.next = next; }
	public int getID() {	return ID; }
	public Belong getNext() {return next; }
}

class ElmtFileTracker
{
	private int ID; //ID File pada tracker
	private String NameFile; //nama file
	private String HashFile; //hash file, dijadikan kunci
	private Belong First; //linked list ke belong
	private ElmtFileTracker next;
	
	public ElmtFileTracker(int ID, String NameFile, String HashFile)
	{
		this.ID = ID;
		this.NameFile = NameFile;	
		this.HashFile = HashFile;
		First = null;
		next = null;
	}

	public boolean IsBelongEmpty() { return (First == null); }

	public boolean IsBelongExists(int IDPeer)
	{
		/* I.S List tidak mungkin kosong */
		/* F.S Mengembalikan true apabila IDPeer ada, false apabila tidak ada */
		Belong P = First;
		while (P != null)
		{
			if (P.getID() == IDPeer)
			{
				return true;
			}
			P = P.getNext();
		}
		return false;
	}

	public Belong getFirst() {return First; }

	public void InsertBelongFirst(int ID)
	{
		/* I.S ID merupakan Nomor Peer */
		/* F.S memasukkan belong file peer kedalam list */
		if (!isBelongExists(ID))
		{
			Belong B = new Belong(ID);
			B.setNext(First);
			First = B;
		}	
	}

	public void DeleteBelong(int ID)
	{
		if (!IsBelongEmpty())
		{
			Belong P = First;
			Belong prev = null;
			if (First.getID() == ID)
			{
				First = P.getNext();
			}
			else
			{
				prev = P;
				P = P.getNext();
				while (P != null)
				{
					if (P.getID() == ID)
					{
						prev.setNext(P.getNext());
					}
					prev = P;
					P = P.getNext();
				}
			}
		}
	}

	public boolean isBelongExists(int ID)
	{
		Belong P = First;
		while (P != null)
		{
			if (P.getID() == ID)
				return true;
			
			P = P.getNext();
		}
		return false;
	}

	/* Setter */
	public void setNext(ElmtFileTracker Next) {this.next = Next; }
	/* End of Setter */

	/* Getter */	
	public int getID() { return ID;}
	public String getName(){return NameFile;}
	public String getHash(){return HashFile;}
	public ElmtFileTracker getNext(){return next;}
	/* End of Getter */

	public void print()
	{
		Belong P = First;
		System.out.print("Nama file : "+NameFile+" ");
		while (P != null)
		{
			System.out.print(P.getID()+" ");
			P = P.getNext();
		}
		System.out.println("");
	}
}

class FileTracker
{
	private static ElmtFileTracker First;
	private static int Top;

	public FileTracker()
	{
		First = null;
		Top = 0;
	}
	
	public static boolean isEmpty() { return (First == null); }
	public static ElmtFileTracker getFirst() {return First;}

	public static ElmtFileTracker SearchExists(String HashFile)
	{
		/* I.S HashFile yang dijadikan pencocok terdefinisi */
		/* F.S mengembalikan Alamat ElmtFileTracker apabila hashfile ditemukan, null apabila tidak ditemukan */
		ElmtFileTracker P = First;
		while (P != null)
		{
			if (P.getHash().equals(HashFile))
			{
				return P;
			}
			P = P.getNext();
		}
		return P;	
	}
					

	public static void DeleteElmt(ElmtFileTracker Addr)
	{
		/* I.S List dijamin tidak kosong, delete Elemen yang memiliki address tertentu */
		/* F.S List bisa berkurang atau tetap */
		if (Addr == First) //jika first
		{
			First = Addr.getNext();
		}
		else
		{
			ElmtFileTracker P = First;
			ElmtFileTracker prev = P;
			P = P.getNext();			
			while (P != null)
			{
				if (P == Addr)
				{
					prev.setNext(P.getNext());
				}
				prev = P;
				P = P.getNext();
			}
		}
	}

	public static void DeleteAllEmpty()
	{
		//I.S List bisa saja kosong
		//F.S Semua elemen File Tracker yang belongnya "null" akan dihapus
		ElmtFileTracker P = First;
		while (P != null)
		{
			if (P.IsBelongEmpty())
			{
				DeleteElmt(P);
			}
			P = P.getNext();
		}
	}
	
	public static void DeleteAllBelong(int ID) //ID Peer
	{
		//Menghapus semua kepunyaan file yang tercatat ID Peer yang terputus
		ElmtFileTracker P = First;
		while (P != null)
		{
			P.DeleteBelong(ID);
			P = P.getNext();
		}
	}

	public static void Insert(int IDPeer, String NameFile, String HashFile)
	{
		ElmtFileTracker P = SearchExists(HashFile);
		if (P == null)
		{
			Top++;
			ElmtFileTracker e = new ElmtFileTracker(Top, NameFile, HashFile);
			e.setNext(First);
			First = e;
			e.InsertBelongFirst(IDPeer);
		}
 		else //jika file ini unik (Hashfile tidak ada yang sama)
		{
			P.InsertBelongFirst(IDPeer);
		}
	}

	public static String SearchNameFile(int ID)
	{
		// I.S List boleh kosong
		// F.S Mengembalikan nama file berdasarkan ID, kembalian NULL apabila ID tidak ada
		String ret = "NULL";
		ElmtFileTracker P = First;
		while (P != null)
		{
			if (P.getID() == ID)
			{
				ret = P.getName();
			}
			P = P.getNext();
		}
		return ret;
	}

	public static void printAll()
	{
		ElmtFileTracker P = First;
		while (P != null)
		{
			P.print();
			P = P.getNext();
		}
	}

	public static void GenerateTorrent()
	{
		ElmtFileTracker P = First;
		
		while (P != null)
		{
			String content = "";
			try
			{	
				Belong B = P.getFirst();
				int k = 1;
				while (B != null)
				{	
					Integer I = new Integer(ListUser.SearchPort(B.getID()));
					content = content + ListUser.SearchIP(B.getID()) + "-" +I.toString() + "-" + P.getHash()+"-"+k+"\n";
					B = B.getNext();
					k++;
				}				
				String Lokasi = "Files/"+P.getName()+".tr";
				File file = new File(Lokasi);

				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(content);
				bw.close();
				P = P.getNext();
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				P = P.getNext();
			}		
		}
	}
}

public class MainListFileTracker
{
	public static void main(String args[])
	{
		new FileTracker();
		FileTracker.Insert(1,"habibie","1230uasda");
		FileTracker.Insert(2,"ganteng","0938409jf");
		FileTracker.Insert(3,"habas","1230uasda");
		ElmtFileTracker P = FileTracker.SearchExists("1230uasda");
		P.print();
	}
}

	
		
