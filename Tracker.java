/*****
Created By : Habibie Faried
Date : 12 Juni 2013
Deskripsi : File ini merupakan kode untuk tracker hTorrent
******/

import java.net.*;
import java.io.*;
import java.util.Scanner;


class TrackerThread extends Thread
{
	private ServerSocket serverSocket;
	private int ID;
	private String nama;

	public TrackerThread(ServerSocket serverSocket, int ID)
	{
		this.serverSocket = serverSocket;
		this.ID = ID;
	}
	
	public void run()
	{
		boolean isDone = false;
		try
		{
			String data; String command;
			Socket server = serverSocket.accept();
			Algorithm A = new Algorithm(server);
			System.out.println("Connected to "+ server.getInetAddress().getHostAddress());

			/* Tersambung ke tracker */
			nama = A.recv();
			int port = Integer.parseInt(A.recv());
			System.out.println("Koneksi ini dilakukan oleh : "+nama);
			System.out.print("bash> ");
			A.cmd("clear");
			A.send("Anda telah terhubung ke tracker");
			ListUser.InsertFirst(ID,server.getInetAddress().getHostAddress(),nama,port); //daftarkan client yang sudah masuk

			/* Terima semua list file dari peer */
			boolean isTerima = false;
			while (!isTerima)
			{
				String name = A.recv();
				if (name.equals("Done"))
					isTerima = true; //lalu keluar
				else
				{
					String hash = A.recv();
					FileTracker.Insert(ID,name,hash);
				}
			}
			FileTracker.GenerateTorrent(); //generate file torrent
			/* End of Terima */
			
			/* Sekuens perintah selanjutnya */
			while (!isDone)
			{
				try
				{
					command = A.recv();
					if (command.equals("quit"))
						isDone = true;
					else if (command.equals("ListFile"))
					{
						ElmtFileTracker P = FileTracker.getFirst();
						while (P != null)
						{
							if (!P.IsBelongExists(ID)) //jika file bukan milik dia
							{
								Integer AI = new Integer(P.getID());
								A.send(P.getName()); //send name file
								A.send(AI.toString());
							}
							P = P.getNext();
						}
						A.send("ListDone");
						A.send("Dummy");
					}
					else if (command.equals("download"))
					{
						int id = Integer.parseInt(A.recv());
						String nama = FileTracker.SearchNameFile(id);
						A.send(nama); //send nama file
						if (!nama.equals("NULL"))
						{
							String FilePath = "Files/"+nama+".tr";
							String FileName = nama+".tr";							
							A.sendFile(FilePath); //sending torrent file
						}
					}
					else if (command.equals("StoreList"))
					{
						/* Terima semua list file dari peer */
						isTerima = false;
						FileTracker.DeleteAllBelong(ID); //hapus semua file kepunyaan peer ini
						FileTracker.DeleteAllEmpty();
						while (!isTerima)
						{
							String name = A.recv();
							if (name.equals("Done"))
								isTerima = true; //lalu keluar
							else
							{
								String hash = A.recv();
								FileTracker.Insert(ID,name,hash);
							}
						}
						FileTracker.GenerateTorrent(); //generate file torrent
						/* End of Terima */
					}
					else
					{
						System.out.println("Kesalahan perintah, ada serangan!");
						System.out.print("bash> ");
						isDone = true;
					}
				}
				catch (Exception e) //jika terjadi error pada transfer msg/data
				{
					System.out.println("Error data : "+e.getMessage());
				}
			}
			/* End of Sekuens */
			
			server.close();
		}
		catch (Exception e)
		{
			System.out.println("TrackerThread Error : "+e.getMessage());
		}
		ListUser.Delete(ID); //delete ID
		FileTracker.DeleteAllBelong(ID); //hapus semua file kepunyaan peer ini
		FileTracker.DeleteAllEmpty();
		FileTracker.GenerateTorrent(); //generate file torrent
	}
}

public class Tracker
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		String command;

		System.out.print("Masukkan berapa thread yang dipanggil : "); int jml = sc.nextInt();
		int i = 1;
		new ListUser();
		new FileTracker();

		try
		{		
			ServerSocket serverSocket = new ServerSocket(5050);		
			while (i <= jml)
			{
				new TrackerThread(serverSocket,i).start();
				i++;		
			}
			System.out.println("Listening peer @ "+serverSocket.getLocalPort());
						
			while (true)
			{
				System.out.print("bash> "); command = sc.nextLine();
				if (command.equals("ListUser"))
					ListUser.print();
				else if (command.equals("ListFile"))
					FileTracker.printAll();
				else if (command.equals("quit"))
					System.exit(0);
				else
					System.out.println("");
			}
			
		}
		catch (Exception e) { System.out.println("Main Tracker Error : "+e.getMessage()); }
		
	}
}	
			
